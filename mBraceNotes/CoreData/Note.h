//
//  Note.h
//  mBraceNotes
//
//  Created by Jorge Salgado on 18/05/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Note : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * text;

@end
