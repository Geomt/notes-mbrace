//
//  DetailViewController.h
//  mBraceNotes
//
//  Created by Jorge Salgado on 18/05/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Note;

@interface DetailViewController : UIViewController
@property (nonatomic, strong) Note *note;

@end
