//
//  DetailViewController.m
//  mBraceNotes
//
//  Created by Jorge Salgado on 18/05/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import "DetailViewController.h"
#import "Note.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextView *noteTextView;

- (void)configureView;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setNote:(Note *)note
{
    if (_note != note) {
        _note = note;
        
        // Update the view.
        [self configureView];
    }
}

#pragma mark - View related methods

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.note) {
        self.noteTextView.text = [self.note text];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}


@end
