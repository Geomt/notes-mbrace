# Notes mBrace
-----

This code corresponds to __Version 1__. It has UITableVIew with basic UITableViewCells and an additional DetailViewController to see the full content of the note.

__IMPORTANT:__ since this project uses CocoaPods, please open it always using the .xcworkspace file and __NOT__ the .xcodeproj