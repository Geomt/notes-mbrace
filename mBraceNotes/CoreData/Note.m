//
//  Note.m
//  mBraceNotes
//
//  Created by Jorge Salgado on 18/05/14.
//  Copyright (c) 2014 Jorge Salgado. All rights reserved.
//

#import "Note.h"


@implementation Note

@dynamic id;
@dynamic text;

@end
